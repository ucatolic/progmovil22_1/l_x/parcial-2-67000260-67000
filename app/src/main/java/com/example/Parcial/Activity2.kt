package com.example.parcial

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi

import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import com.example.parcial.R


class Activity2 : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity2)
        var flag = false


        //spinner 2 (Proteinas)
        val spn2: Spinner = findViewById(R.id.sp2)
        val proteinas = listOf<String>("Angus", "Chicken", "veggie", "pork", "Turkey")
        val adaptador = ArrayAdapter(this, android.R.layout.simple_spinner_item, proteinas)
        spn2.adapter = adaptador
        //emparejar el sppiner de la proteina
        val valorSpinner = spn2.selectedItemPosition
        val valorEnArreglo = proteinas.get(valorSpinner)

        //vegetales
        val onion: CheckBox = findViewById(R.id.ch1)
        val tomaco: CheckBox = findViewById(R.id.ch2)
        val root: CheckBox = findViewById(R.id.ch3)
        val allibum: CheckBox = findViewById(R.id.ch4)
        val No_Veggatables: CheckBox = findViewById(R.id.ch5)
        var Orden: String = "Orden"


        //sauces
        val mayonaise: CheckBox = findViewById(R.id.chSauces1)
        val ketcup: CheckBox = findViewById(R.id.chSauces2)
        val honney: CheckBox = findViewById(R.id.chSauces3)
        val noneSauce: CheckBox = findViewById(R.id.chSauces4)
        var sauces: String = ""

        //boton de ordenar
        val botton: Button = findViewById(R.id.Ordenar_boton)


        //bread
        val bread: RadioGroup = findViewById(R.id.BreadOption)
        val integral: RadioButton = findViewById(R.id.Bread1)
        val comapan: RadioButton = findViewById(R.id.Bread2)
        val panBlanco: RadioButton = findViewById(R.id.Bread3)
        val viena: RadioButton = findViewById(R.id.Bread4)
        var pan: String? = null

        //papas y demas
        val papasYMas: CheckBox = findViewById(R.id.chChips)
        val sinpapasYMas: CheckBox = findViewById(R.id.none)
        var aditionals:String = ""

        // si hace click en no vegetables
        fun papasYmas(): String {

            if (sinpapasYMas.isChecked) {
                aditionals = "No chips "

            } else if (!sinpapasYMas.isChecked) {
                papasYMas.setEnabled(true)

                if (papasYMas.isChecked) {
                    aditionals = "$aditionals Chips and soda,"
                }
            }
            return aditionals

        }




        fun pan(): String? {
            bread.setOnCheckedChangeListener { radioGroup: RadioGroup, i: Int ->
                if (integral.isChecked) {
                    pan = "Integral"
                }
                if (comapan.isChecked) {
                    pan = "comapan"
                }
                if (panBlanco.isChecked) {
                    pan = "Pan blanco"
                }
                if (viena.isChecked) {
                    pan = "Viena"
                }

            }
            return pan
        }

        // si hace click en no vegetables
        fun vegetales(): String {

            if (No_Veggatables.isChecked) {
                Orden = "No vegetales"

            } else if (!No_Veggatables.isChecked) {
                onion.setEnabled(true)
                tomaco.setEnabled(true)
                root.setEnabled(true)
                allibum.setEnabled(true)


                if (onion.isChecked) {
                    Orden = "$Orden onion,"
                }
                if (tomaco.isChecked) {
                    Orden = "$Orden tomaco,"

                }
                if (root.isChecked) {
                    Orden = "$Orden root,"

                }
                if (allibum.isChecked) {
                    Orden = "$Orden allibum,"

                }

            }
            return Orden

        }

        fun Sauces(): String {

            if (noneSauce.isChecked) {
                Orden = "No Sauce"

            } else if (!noneSauce.isChecked) {
                mayonaise.setEnabled(true)
                ketcup.setEnabled(true)
                honney.setEnabled(true)
                noneSauce.setEnabled(true)


                if (mayonaise.isChecked) {
                    sauces = "$sauces mayonaise,"
                }
                if (ketcup.isChecked) {
                    sauces = "$sauces ketcup,"

                }
                if (honney.isChecked) {
                    sauces = "$sauces honney,"

                }
                if (noneSauce.isChecked) {
                    sauces = "$sauces noneSauce,"

                }

            }
            return sauces

        }


        fun msg(msg: String) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        }

        fun lookValue() {
            if (valorEnArreglo.isNotBlank()) {
                var dialogo: String = "Order:\n" +
                        "Protein:\t${valorEnArreglo}\n" +
                        "Bread:\t${pan()}\n" +
                        "Vegetales:\t${vegetales()}\n" +
                        "Sauces:\t${Sauces()}\n"+
                        "Aditionals:\t${papasYmas()}"
                val intent = Intent(this, Dialog3::class.java)
                intent.putExtra("INTENT_dialogo", dialogo)
                startActivity(intent)
                flag = true
            } else {
                msg("check values and try again")
            }
        }






        botton.setOnClickListener { lookValue() }
//        val intent = Intent(this, Dialog3::class.java)
//        intent.putExtra("INTENT_dialogo", Orden)
//        startActivity(intent)

    }

}

private fun RadioGroup.setOnCheckedChangeListener(function: () -> Unit) {

}
