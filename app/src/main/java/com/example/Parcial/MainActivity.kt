package com.example.parcial

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.view.MenuItem
import android.widget.*
import com.example.parcial.Activity2

class MainActivity : AppCompatActivity() {
    var flag = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        val guardar:Button = findViewById(R.id.button)
        guardar.setOnClickListener {
            lookValue()
        }
        val spn1:Spinner=findViewById(R.id.spinner)
        val phone= listOf<String>("Mobile", "Phone")
        val adaptador=ArrayAdapter(this,android.R.layout.simple_spinner_item,phone)
        spn1.adapter= adaptador
        spn1.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                Toast.makeText(this@MainActivity, p2.toString(), Toast.LENGTH_SHORT).show()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
            }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.User->{
            lookValue()
            true
        }
        R.id.Developer->{
            val intent = Intent(this, Dialog2::class.java)
            startActivity(intent)
            true
        }
        R.id.Order->{
            if(flag){
                val intent = Intent(this, Activity2::class.java)
                startActivity(intent)
            }else {
                msg("check values and try again")
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    fun msg(msg:String){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
    fun lookValue() {
        val etName = findViewById<EditText>(R.id.idName)
        val etPhone = findViewById<EditText>(R.id.idPhone)
        val etAddress = findViewById<EditText>(R.id.idAddress)
        val etMail = findViewById<EditText>(R.id.idMail)

        if (etName.text.isNotEmpty() && etPhone.text.isNotEmpty() && etAddress.text.isNotEmpty() && etMail.text.isNotEmpty()) {
            var dialogo:String="CLIENT DATA:\n" +
                    "NAME:\n${etName.text}\n" +
                    "PHONE:\n${etPhone.text}\n" +
                    "ADDRESS:\n${etAddress.text}\n" +
                    "EMAIL:\n${etMail.text}"
            val intent = Intent(this, Dialog1::class.java)
            intent.putExtra("INTENT_dialogo", dialogo)
            startActivity(intent)
            flag = true
        } else {
            msg("check values and try again")
        }
    }

}