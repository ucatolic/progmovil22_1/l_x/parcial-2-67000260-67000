package com.example.parcial

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class Dialog3: AppCompatActivity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog3)
        msg()
    }
    fun msg(){
        val bundle=intent.extras
        val dial = bundle?.get("INTENT_dialogo")
        val ShowMsg: TextView =findViewById(R.id.dialog1)
        ShowMsg.text="$dial"

    }
}